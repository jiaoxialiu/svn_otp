package com.demo.svnotp.auth;

import com.demo.svnotp.setting.OtpSetting;
import com.demo.svnotp.setting.OtpSettingConfigurable;
import com.demo.svnotp.util.OtpProjectUtil;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ModalityState;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.util.Ref;
import com.intellij.openapi.util.text.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.idea.svn.SvnVcs;
import org.jetbrains.idea.svn.api.Url;
import org.jetbrains.idea.svn.auth.*;
import org.jetbrains.idea.svn.dialogs.SSLCredentialsDialog;
import org.jetbrains.idea.svn.dialogs.ServerSSLDialog;
import org.jetbrains.idea.svn.dialogs.SimpleCredentialsDialog;

import java.security.cert.X509Certificate;

import static com.intellij.openapi.vcs.ui.VcsBalloonProblemNotifier.showOverChangesView;
import static org.jetbrains.idea.svn.SvnBundle.message;

/**
 * Description: OTP code 认证；
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpSvnInteractiveAuthenticationProvider extends SvnInteractiveAuthenticationProvider {

    private static final Logger LOG = Logger.getInstance(OtpSvnInteractiveAuthenticationProvider.class);

    @NotNull
    private final SvnVcs myVcs;

    @NotNull
    private final Project myProject;

    private static final ThreadLocal<SvnInteractiveAuthenticationProvider.MyCallState> myCallState = new ThreadLocal<>();

    private final SvnAuthenticationManager myManager;

    public OtpSvnInteractiveAuthenticationProvider(@NotNull SvnVcs vcs, SvnAuthenticationManager manager) {
        super(vcs, manager);
        myManager = manager;
        myVcs = vcs;
        myProject = vcs.getProject();
    }

    @Override
    public AuthenticationData requestClientAuthentication(final String kind,
                                                          final Url url,
                                                          final String realm,
                                                          final boolean canCache) {
        final SvnInteractiveAuthenticationProvider.MyCallState callState = new SvnInteractiveAuthenticationProvider.MyCallState(true, false);
        // myCallState.set(callState);
        // once we came here, we don't know _correct_ auth todo +-
        myVcs.getSvnConfiguration().clearCredentials(kind, realm);

        final AuthenticationData[] result = new AuthenticationData[1];
        Runnable command = null;

        final boolean authCredsOn = canCache && myManager.getHostOptions(url).isAuthStorageEnabled();
        if (SvnAuthenticationManager.PASSWORD.equals(kind)) {
            // 替换为OTP user_name
            String userName = OtpSetting.getInstance().getUserName();
            if (userName == null) {
                userName = myManager.getDefaultUsername();
            }
            String finalUserName = userName;
            command = () -> {
                String otpCode = OtpProjectUtil.getCode();
                if (otpCode != null && !otpCode.isEmpty()) {
                    result[0] = new PasswordAuthenticationData(finalUserName, otpCode, false);
                } else {
                    String content = OtpProjectUtil.message("otp.get_code.err");
                    NotificationType type = NotificationType.INFORMATION;
                    Notification switchInform = new Notification(OtpSettingConfigurable.id, OtpSettingConfigurable.id, content, type);
                    Notifications.Bus.notify(switchInform, myProject);

                    SimpleCredentialsDialog dialog = new SimpleCredentialsDialog(myProject);
                    dialog.setup(realm, finalUserName, authCredsOn);
                    dialog.setTitle(message("dialog.title.authentication.required"));

                    if (dialog.showAndGet()) {
                        result[0] = new PasswordAuthenticationData(dialog.getUserName(), dialog.getPassword(), dialog.isSaveAllowed());
                    }
                }
            };
        } else if (SvnAuthenticationManager.SSL.equals(kind)) {
            command = () -> {
                SvnAuthenticationManager.HostOptions options = myManager.getHostOptions(url);
                final String file = options.getSSLClientCertFile();
                final SSLCredentialsDialog dialog = new SSLCredentialsDialog(myProject, realm, authCredsOn);
                if (!StringUtil.isEmptyOrSpaces(file)) {
                    dialog.setFile(file);
                }
                dialog.setTitle(message("dialog.title.authentication.required"));

                if (dialog.showAndGet()) {
                    result[0] = new CertificateAuthenticationData(dialog.getCertificatePath(), dialog.getCertificatePassword(), dialog.getSaveAuth());
                }
            };
        }

        if (command != null) {
            showAndWait(command);
            LOG.debug("3 authentication result: " + result[0]);
        }

        final boolean wasCanceled = result[0] == null;
        callState.setWasCancelled(wasCanceled);
        return result[0];
    }

    @Override
    public AcceptResult acceptServerAuthentication(final Url url,
                                                   String realm,
                                                   final Object certificate,
                                                   final boolean canCache) {
        Ref<AcceptResult> result = Ref.create(AcceptResult.REJECTED);
        Runnable command;
        if (certificate instanceof X509Certificate || certificate instanceof String) {
            command = () -> {
                ServerSSLDialog dialog = certificate instanceof X509Certificate
                        ? new ServerSSLDialog(myProject, (X509Certificate) certificate, canCache)
                        : new ServerSSLDialog(myProject, (String) certificate, canCache);
                dialog.show();
                result.set(dialog.getResult());
            };
        } else {
            showOverChangesView(myProject, message("notification.content.unknown.certificate.type.from.url", url.toDecodedString()), MessageType.ERROR);
            return AcceptResult.REJECTED;
        }

        showAndWait(command);
        return result.get();
    }

    private static void showAndWait(@NotNull Runnable command) {
        ApplicationManager.getApplication().invokeAndWait(command, ModalityState.any());
    }

}
