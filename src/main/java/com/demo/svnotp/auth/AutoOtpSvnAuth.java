package com.demo.svnotp.auth;

import com.demo.svnotp.util.OtpProjectUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.idea.svn.RootUrlInfo;
import org.jetbrains.idea.svn.SvnConfiguration;
import org.jetbrains.idea.svn.SvnFileUrlMapping;
import org.jetbrains.idea.svn.SvnVcs;
import org.jetbrains.idea.svn.auth.SvnAuthenticationManager;
import org.jetbrains.idea.svn.auth.SvnInteractiveAuthenticationProvider;

/**
 * Description: 设置OTP code验证提供者
 *
 * @author twj
 * @date 2023-11-20
 */
public class AutoOtpSvnAuth {

    private static void otpSvnAuth(Project project) {
        if (project == null) {
            return;
        }
        if (!OtpProjectUtil.configured()) {
            return;
        }
        // 项目svn 实例
        SvnVcs svnVcs = SvnVcs.getInstance(project);
        if (svnVcs == null) {
            return;
        }
        // TODO 暂时没找到合适的方法判断项目的仓库类型为SVN
        SvnConfiguration instance = SvnConfiguration.getInstance(project);
        SvnAuthenticationManager interactiveManager = instance.getInteractiveManager(svnVcs);
        if (interactiveManager.getProvider() instanceof OtpSvnInteractiveAuthenticationProvider) {
            return;
        }
        SvnAuthenticationManager authenticationManager = instance.getAuthenticationManager(svnVcs);
        // 自定义AuthenticationProvider覆盖密码弹窗
        OtpSvnInteractiveAuthenticationProvider authenticationProvider = new OtpSvnInteractiveAuthenticationProvider(svnVcs, authenticationManager);
        interactiveManager.setAuthenticationProvider(authenticationProvider);
        authenticationManager.setAuthenticationProvider(authenticationProvider);
    }

    public static synchronized void otpSvnAuth() {
        ProjectManager projectManager = ProjectManager.getInstance();
        @NotNull Project[] openProjects = projectManager.getOpenProjects();
        if (openProjects.length < 1) {
            return;
        }
        for (Project p : openProjects) {
            otpSvnAuth(p);
        }
    }

    /**
     * 还原成默认的认证方式
     *
     * @return
     */
    public synchronized static void nativeAuth() {
        ProjectManager projectManager = ProjectManager.getInstance();
        @NotNull Project[] openProjects = projectManager.getOpenProjects();
        for (Project project : openProjects) {
            SvnVcs svnVcs = SvnVcs.getInstance(project);
            SvnConfiguration configuration = SvnConfiguration.getInstance(project);
            SvnAuthenticationManager interactiveManager = configuration.getInteractiveManager(svnVcs);
            SvnAuthenticationManager authenticationManager = configuration.getAuthenticationManager(svnVcs);
            // 设置成svn插件默认的模式
            SvnInteractiveAuthenticationProvider myInteractiveProvider = new SvnInteractiveAuthenticationProvider(svnVcs, interactiveManager);
            interactiveManager.setAuthenticationProvider(myInteractiveProvider);
            authenticationManager.setAuthenticationProvider(myInteractiveProvider);
            // 清除认证结果缓存
            SvnConfiguration svnConfiguration = SvnConfiguration.getInstance(project);
            SvnFileUrlMapping svnFileUrlMapping = svnVcs.getSvnFileUrlMapping();
            for (RootUrlInfo urlInfo : svnFileUrlMapping.getAllWcInfos()) {
                svnConfiguration.clearCredentials(SvnAuthenticationManager.PASSWORD, urlInfo.getRepositoryUrl().toString());
            }
        }
    }

    public static boolean otpOpened(Project project) {
        if (project == null) {
            return false;
        }
        if (!OtpProjectUtil.configured()) {
            return false;
        }
        // 项目svn 实例
        SvnVcs svnVcs = SvnVcs.getInstance(project);
        if (svnVcs == null) {
            return false;
        }
        SvnConfiguration instance = SvnConfiguration.getInstance(project);
        SvnAuthenticationManager interactiveManager = instance.getInteractiveManager(svnVcs);
        return interactiveManager.getProvider() instanceof OtpSvnInteractiveAuthenticationProvider;
    }

}
