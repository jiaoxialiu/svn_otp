package com.demo.svnotp.setting;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Description: OTP 设置界面
 *
 * @author twj
 * @date 2023-11-21
 */
public class OtpSettingForm {

    private JTextField userNameTextField;

    private JPasswordField secretPasswordField;

    public JPanel mainPanel;

    private JTextField codeCacheTimeTextField;

    public OtpSettingForm() {
    }

    public JTextField getUserNameTextField() {
        return userNameTextField;
    }

    public JPasswordField getSecretPasswordField() {
        return secretPasswordField;
    }

    public JTextField getCodeCacheTimeTextField() {
        return codeCacheTimeTextField;
    }

    public String getUserName() {
        return userNameTextField.getText();
    }

    public String getSecret() {
        return new String(secretPasswordField.getPassword());
    }

    public Integer getCodeCacheTime() {
        if (codeCacheTimeTextField.getText() == null) {
            return 0;
        }
        return Integer.valueOf(codeCacheTimeTextField.getText());
    }

    public void codeCacheTimeKeyListener() {
        codeCacheTimeTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                int keyChar = e.getKeyChar();
                // 屏蔽掉非法输入
                if (keyChar < KeyEvent.VK_0 || keyChar > KeyEvent.VK_9) {
                    e.consume();
                }
            }
        });
    }

}
