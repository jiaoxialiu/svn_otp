package com.demo.svnotp.setting;

import com.demo.svnotp.auth.AutoOtpSvnAuth;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.util.NlsContexts;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Objects;

/**
 * Description: SVN OTP设置界面
 *
 * @author twj
 * @date 2023-11-21
 */
public class OtpSettingConfigurable implements SearchableConfigurable {

    public static final String id = "SVN OTP";

    private final OtpSetting otpSetting;

    private OtpSettingForm otpSettingForm;

    public static OtpSettingConfigurable getInstance() {
        return ApplicationManager.getApplication().getComponent(OtpSettingConfigurable.class);
    }

    public OtpSettingConfigurable() {
        this.otpSetting = OtpSetting.getInstance();
    }

    @Override
    public @NotNull @NonNls String getId() {
        return id;
    }

    @Override
    public @NlsContexts.ConfigurableName String getDisplayName() {
        return getId();
    }

    @Override
    public @Nullable JComponent createComponent() {
        if (otpSettingForm == null) {
            otpSettingForm = new OtpSettingForm();
            otpSettingForm.codeCacheTimeKeyListener();
        }
        if (otpSetting.getUserName() != null) {
            otpSettingForm.getUserNameTextField().setText(otpSetting.getUserName());
        }
        if (otpSetting.getSecret() != null) {
            otpSettingForm.getSecretPasswordField().setText(otpSetting.getSecret());
        }
        if (otpSetting.getCodeCacheTime() != null) {
            otpSettingForm.getCodeCacheTimeTextField().setText(otpSetting.getCodeCacheTime().toString());
        }
        return otpSettingForm.mainPanel;
    }

    @Override
    public boolean isModified() {
        return !Objects.equals(otpSetting.getUserName(), otpSettingForm.getUserName())
                || !Objects.equals(otpSetting.getSecret(), otpSettingForm.getSecret())
                || !Objects.equals(otpSetting.getCodeCacheTime(), otpSettingForm.getCodeCacheTime());
    }

    @Override
    public void apply() throws ConfigurationException {
        String userName = otpSettingForm.getUserName();
        String secret = otpSettingForm.getSecret();
        if (userName != null && secret != null && !userName.isBlank() && !secret.isBlank()) {
            otpSetting.setUserName(userName);
            otpSetting.setSecret(secret);
            otpSetting.setCodeCacheTime(otpSettingForm.getCodeCacheTime());
            AutoOtpSvnAuth.otpSvnAuth();
        }
    }

    @Override
    public void reset() {
        otpSettingForm.getUserNameTextField().setText(otpSetting.getUserName());
        otpSettingForm.getSecretPasswordField().setText(otpSetting.getSecret());
        otpSettingForm.getCodeCacheTimeTextField().setText(otpSetting.getCodeCacheTime().toString());
        SearchableConfigurable.super.reset();
    }

    @Override
    public void disposeUIResources() {
        otpSettingForm.mainPanel.setEnabled(false);
        otpSettingForm.mainPanel = null;
        SearchableConfigurable.super.disposeUIResources();
    }

}
