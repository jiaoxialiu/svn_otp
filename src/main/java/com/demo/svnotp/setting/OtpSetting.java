package com.demo.svnotp.setting;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Description:
 *
 * @author twj
 * @date 2023-11-20
 */
@State(name = "otp", storages = {@Storage(value = "$APP_CONFIG$/otp.xml")})
public class OtpSetting implements PersistentStateComponent<OtpSetting> {

    private String userName;

    private String secret;

    private Integer codeCacheTime = 24;

    public static OtpSetting getInstance() {
        return ApplicationManager.getApplication().getService(OtpSetting.class);
    }

    @Override
    public @Nullable OtpSetting getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull OtpSetting state) {
        this.userName = state.userName;
        this.secret = state.secret;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setCodeCacheTime(Integer codeCacheTime) {
        this.codeCacheTime = codeCacheTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getSecret() {
        return secret;
    }

    public Integer getCodeCacheTime() {
        return codeCacheTime;
    }

}
