package com.demo.svnotp.util;

import com.demo.svnotp.setting.OtpSetting;
import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vcs.VcsRoot;
import org.apache.commons.codec.binary.Base32;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.idea.svn.SvnVcs;

import javax.crypto.spec.SecretKeySpec;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.ResourceBundle;

/**
 * Description: OTP插件工具类
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpProjectUtil {

    public static final ResourceBundle otpBundle = ResourceBundle.getBundle("svn_otp");

    private static final TimeBasedOneTimePasswordGenerator totp = new TimeBasedOneTimePasswordGenerator();

    private static String code = null;

    // 最近一次获取code的时间
    private static long getCodeTime = 0L;

    public static String message(String key) {
        return otpBundle.getString(key);
    }

    public static String message(String key, Object... param) {
        return MessageFormat.format(otpBundle.getString(key), param);
    }

    public static String messageAndParamKey(String key, String... paramKey) {
        if (paramKey == null || paramKey.length < 1) {
            return message(key);
        }
        Object[] arguments = new Object[paramKey.length];
        for (int i = 0; i < paramKey.length; i++) {
            String k = paramKey[i];
            arguments[i] = otpBundle.getObject(k);
        }
        return MessageFormat.format(otpBundle.getString(key), arguments);
    }

    /**
     * 项目是否配置了svn，必须在项目加载完成之后使用，否则可能获取不到Vcs信息
     *
     * @param project 项目对象
     * @return true 项目使用了SVN
     */
    public static boolean isSvn(@NotNull Project project) {
        ProjectLevelVcsManager vcsManager = ProjectLevelVcsManager.getInstance(project);
        VcsRoot[] vcsRoots = vcsManager.getAllVcsRoots();
        for (VcsRoot vcsRoot : vcsRoots) {
            if (vcsRoot.getVcs() instanceof SvnVcs) {
                return true;
            }
        }
        return false;
    }

    /**
     * OTP 用户信息是否已配置
     *
     * @return true 是
     */
    public static boolean configured() {
        OtpSetting otpSetting = OtpSetting.getInstance();
        String userName = otpSetting.getUserName();
        String secret = otpSetting.getSecret();
        return userName != null && secret != null && !userName.isBlank() && !secret.isBlank();
    }

    /**
     * 获取OTP code
     *
     * @return code or null
     */
    public static String getCode() {
        OtpSetting otpSetting = OtpSetting.getInstance();
        Long codeCacheTime = Long.valueOf(otpSetting.getCodeCacheTime());
        if (codeCacheTime > 0 && code != null && getCodeTime > 0) {
            long now = System.currentTimeMillis();
            if (now <= getCodeTime + codeCacheTime * 3600000L) {
                return code;
            }
        }
        getCodeTime = System.currentTimeMillis();
        return getCodeByGenerate(otpSetting.getUserName(), otpSetting.getSecret());
    }

    public static String getCodeByGenerate(String username, String secret) {
        try {
            // 创建 Base32 解码器实例
            Base32 base32 = new Base32();
            // 将 Base32 编码的字符串解码为字节数组
            byte[] decodedBytes = base32.decode(secret);
            SecretKeySpec key = new SecretKeySpec(decodedBytes, "HmacSHA1");

            code = totp.generateOneTimePasswordString(key, Instant.now());
            return code;
        } catch (Exception ignore) {
            return null;
        }
    }

}
