package com.demo.svnotp.listener;

import com.demo.svnotp.auth.AutoOtpSvnAuth;
import com.intellij.dvcs.repo.VcsRepositoryMappingListener;
import com.intellij.openapi.diagnostic.Logger;

/**
 * Description: 监听项目VcsRepository更新事件
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpVcsRepListener implements VcsRepositoryMappingListener {

    private static final Logger LOG = Logger.getInstance(OtpVcsRepListener.class);

    @Override
    public void mappingChanged() {
        AutoOtpSvnAuth.otpSvnAuth();
    }

}
