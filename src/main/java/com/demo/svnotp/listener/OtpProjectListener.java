package com.demo.svnotp.listener;

import com.demo.svnotp.auth.AutoOtpSvnAuth;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManagerListener;
import org.jetbrains.annotations.NotNull;

/**
 * Description: 监听项目打开事件
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpProjectListener implements ProjectManagerListener {

    private static final Logger LOG = Logger.getInstance(OtpProjectListener.class);

    @Override
    public void projectOpened(@NotNull Project project) {
        AutoOtpSvnAuth.otpSvnAuth();
        // 当项目初始化完成后设置 SVN 配置，触发该事件时项目可能未初始化完成
        // 该方式无法在打开上次打开的文件前完成设置
        // DumbService.getInstance(project).runWhenSmart(() -> AutoOtpSvnAuth.otpSvnAuth(project));
        ProjectManagerListener.super.projectOpened(project);
    }

}
