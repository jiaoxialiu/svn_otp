package com.demo.svnotp.listener;

import com.demo.svnotp.auth.AutoOtpSvnAuth;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * Description: 监听项目文件打开事件
 * 当idea启动打开上次打开的文件时该事件先于VcsRepositoryMappingListener被触发
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpFileEditorListener implements FileEditorManagerListener {

    private static final Logger LOG = Logger.getInstance(OtpFileEditorListener.class);

    /**
     * 文件每打开一个就会触发一次，OTP初始化过了就不再设置
     *
     * @param source
     * @param file
     */
    @Override
    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        AutoOtpSvnAuth.otpSvnAuth();
        FileEditorManagerListener.super.fileOpened(source, file);
    }

}
