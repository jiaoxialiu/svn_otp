package com.demo.svnotp.action;

import com.demo.svnotp.setting.OtpSettingConfigurable;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.options.ShowSettingsUtil;
import org.jetbrains.annotations.NotNull;

import static com.demo.svnotp.util.OtpProjectUtil.message;

/**
 * Description: 打开OTP设置界面Action
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpSettingNotificationAction extends NotificationAction {

    public OtpSettingNotificationAction() {
        super(message("setting.form.open"));
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e, @NotNull Notification notification) {
        // 通过配置项名字，直接显示对应的配置界面
        ShowSettingsUtil.getInstance().showSettingsDialog(e.getProject(), OtpSettingConfigurable.id);
        notification.expire();
    }

}
