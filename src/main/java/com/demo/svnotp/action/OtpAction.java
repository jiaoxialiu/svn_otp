package com.demo.svnotp.action;

import com.demo.svnotp.auth.AutoOtpSvnAuth;
import com.demo.svnotp.setting.OtpSettingConfigurable;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageDialogBuilder;
import org.jetbrains.annotations.NotNull;

import static com.demo.svnotp.util.OtpProjectUtil.*;

/**
 * Description: SVN OTP code Action
 *
 * @author twj
 * @date 2023-11-20
 */
public class OtpAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Notification notification = new Notification(OtpSettingConfigurable.id, message("setting.form.title"), NotificationType.WARNING);
        // 提示进行配置
        if (!configured()) {
            OtpSettingNotificationAction notificationAction = new OtpSettingNotificationAction();
            notification.addAction(notificationAction);
            Notifications.Bus.notify(notification, e.getProject());
            return;
        }
        switchOtp(e.getProject());
    }

    /**
     * 切换项目的认证方式
     *
     * @param project 项目
     */
    private void switchOtp(Project project) {
        // 点击切换校验方式
        boolean otp = AutoOtpSvnAuth.otpOpened(project);
        MessageDialogBuilder.YesNo dialog;
        if (otp) {
            dialog = MessageDialogBuilder.yesNo(message("switch.dialog.title"),
                    messageAndParamKey("switch.dialog.msg", "switch.dialog.msg.on", "switch.dialog.msg.close"));
        } else {
            dialog = MessageDialogBuilder.yesNo(message("switch.dialog.title"),
                    messageAndParamKey("switch.dialog.msg", "switch.dialog.msg.off", "switch.dialog.msg.open"));
        }
        if (dialog.guessWindowAndAsk()) {
            if (otp) {
                AutoOtpSvnAuth.nativeAuth();
                String content = message("switch.dialog.close.notification");
                NotificationType type = NotificationType.INFORMATION;
                Notification switchInform = new Notification(OtpSettingConfigurable.id, OtpSettingConfigurable.id, content, type);
                Notifications.Bus.notify(switchInform, project);
            } else {
                AutoOtpSvnAuth.otpSvnAuth();
                NotificationType type = NotificationType.INFORMATION;
                String content = message("switch.dialog.open.notification");
                Notification switchInform = new Notification(OtpSettingConfigurable.id, OtpSettingConfigurable.id, content, type);
                Notifications.Bus.notify(switchInform, project);
            }
        }
    }

}
